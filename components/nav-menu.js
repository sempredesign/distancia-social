import ImportComponent from "../../libs/import-component.js";

class NavMenu extends HTMLElement {

    constructor() {
        super();
        this._visible = this.getAttribute("visible") === "true";
        this._selecteditem = this.getAttribute("selecteditem");
    }
    
    get visible() {
        return this._visible;
    }
     
    set visible(val) {
        this.setAttribute('visible', val);
    }
     
    static get observedAttributes() { 
        return ['visible', 'selecteditem']; 
    }
    
    attributeChangedCallback(name, oldVal, newVal) {
        switch(name) {
            case 'visible':
                this._visible = newVal === "true" || false;
                this.changeMenu();
                break;
            case "selecteditem":
                this._selecteditem = newVal;
                this.selectItem();
        }
    }
    
    connectedCallback () {
        new ImportComponent("nav-menu", (o) => {
            this.innerHTML = o.target.responseText;
            
            this.changeMenu();
            this.selectItem();
            
            var button = this.querySelector("button");
            button.addEventListener("click", (e) => {
                this.setAttribute("visible", !this._visible);
            });
            button.addEventListener("blur", (e) => { 
                window.setTimeout(() => {
                    this.setAttribute("visible", false);
                }, 250);
            });
            
        });
    }
    
    changeMenu() {
        if (this.classList) {
            this._visible ? this.classList.add('visible') : this.classList.remove('visible');
        }
    }

    selectItem() {
        var items = this.querySelectorAll("li");
        items.forEach(o => { 
            o.classList.remove("selected");
            if (o.classList.contains(this._selecteditem)) {
                o.classList.add("selected");
            };
        });
    }
    
}

customElements.define('nav-menu', NavMenu);
export default NavMenu;
