import ImportComponent from "../../libs/import-component.js";

class TableData extends HTMLElement {

    constructor() {
        super();
        this._selectedTags = this.getAttribute("selectedtags");
    }
    
    get content() {
        return this._content;
    }
    
    set content(val) {
        this._content = val;
        this.renderRows();
    }

    static get observedAttributes() { 
        return ['selectedtags']; 
    }
    
    attributeChangedCallback(name, oldVal, newVal) {
        switch(name) {
            case "selectedtags":
                this._selectedTags = newVal.split(' ');
                this.selectTags();
        }
    }
    
    connectedCallback () {
        new ImportComponent("table-data", (o) => {
            this.innerHTML = o.target.responseText;

            this.selectTags();
            var a = this.querySelectorAll(".tags a");
            a.forEach(o => {
                o.addEventListener("click", (e) => {
                    //add or remove the tag attribute
                    let tag = o.href.split("/").slice(-1)[0];
                    let index = this._selectedTags.indexOf(tag);
                    if (index >= 0) {
                        this._selectedTags.splice(index, 1);
                    } else {
                        this._selectedTags.splice(index, 0, tag);
                    }
                    this.setAttribute("selectedtags", this._selectedTags.join(" "));
                    e.preventDefault();
                    return false;
                });
            });
        });
    }
    
    selectTags() {
        var items = this.querySelectorAll(".tags li");
        items.forEach(o => {
            o.classList.remove("selected");
            let tag = o.querySelector("a").href.split("/").slice(-1)[0];
            this._selectedTags.forEach(t => {
                if (tag === t) {
                    o.classList.add("selected");
                }
            });
        });
    }

    renderRows() {
        let tbody = this.querySelector("tbody");
        let rows = [];
        this._content.forEach((o) => {
            rows.push("<tr>" +
            "<td class='" + (o.locked ? "locked" : "ok") + "'></td>" +
                    "<td><a href='"+o.login+"'>" + o.name + "</a></td>" +
                    "<td><a href='"+o.login+"'>" + o.login + "</a></td>" +
                    "<td><a href='"+o.login+"'>" + o.email + "</a></td>" +
                    "<td><a href='"+o.login+"'>" + o.role + "</a></td>" +
                "</tr>");
        })
        
        tbody.innerHTML = rows.join("\n");
    }
    
}

customElements.define('table-data', TableData);
export default TableData;
