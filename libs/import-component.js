export default class ImportComponent {
    constructor (componentName, callback) {
        const request = new XMLHttpRequest();
        let filename = `./components/${componentName}.html`;
        request.onload = callback;
        request.open("get", filename, true);
        request.send();
    } 
}