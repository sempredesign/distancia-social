import ImportComponent from "../libs/import-component.js";

class LogoMotora extends HTMLElement {
    connectedCallback () {
        new ImportComponent("logo-motora", (o) => {
            this.innerHTML = o.target.responseText;
            this.replaceWith(...this.childNodes);
        });
    }
}

customElements.define('logo-motora', LogoMotora);
export default LogoMotora;