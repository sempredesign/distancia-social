import ImportComponent from "../../libs/import-component.js";

class TabGroup extends HTMLElement {

    constructor() {
        super();
        this._selecteditem = this.getAttribute("selecteditem");
    }
    
    static get observedAttributes() { 
        return ['selecteditem']; 
    }
    
    attributeChangedCallback(name, oldVal, newVal) {
        switch(name) {
            case "selecteditem":
                this._selecteditem = newVal;
                this.selectItem();
        }
    }
    
    connectedCallback () {
        new ImportComponent("tab-group", (o) => {
            this.innerHTML = o.target.responseText;

            this.selectItem();
            var a = this.querySelectorAll("a");
            a.forEach(o => {
                o.addEventListener("click", (e) => {
                    this.setAttribute("selecteditem", o.href.split("/").slice(-1)[0]);
                    e.preventDefault();
                    return false;
                });
            });
        });
    }
    
    selectItem() {
        var items = this.querySelectorAll("li");
        items.forEach(o => { 
            o.classList.remove("selected");
            if (o.querySelector("a").href.split("/").slice(-1)[0] === this._selecteditem) {
                o.classList.add("selected");
            };
        });
    }
    
}

customElements.define('tab-group', TabGroup);
export default TabGroup;
