import ImportComponent from "../../libs/import-component.js";

class UserMenu extends HTMLElement {

    constructor() {
        super();
        this._visible = this.getAttribute("visible") === "true";
    }
    
    get visible() {
        return this._visible;
    }
     
    set visible(val) {
        this.setAttribute('visible', val);
    }
     
    static get observedAttributes() { 
        return ['visible']; 
    }
    
    attributeChangedCallback(name, oldVal, newVal) {
        switch(name) {
            case 'visible':
                this._visible = newVal === "true";
                this.toggleMenu();
                break;
        }
    }
    
    connectedCallback () {
        new ImportComponent("user-menu", (o) => {
            this.innerHTML = o.target.responseText;
            
            this.toggleMenu();

            var button = this.querySelector("button");
            button.addEventListener("click", (e) => {
                this.setAttribute("visible", !this._visible);
            });
            button.addEventListener("blur", (e) => {
                window.setTimeout(() => {
                    this.setAttribute("visible", false);
                }, 250);
            });
            
        });
    }
    
    toggleMenu() {
        if (this.classList) {
            this._visible ? this.classList.add('visible') : this.classList.remove('visible');
        }
    }
    
}

customElements.define('user-menu', UserMenu);
export default UserMenu;